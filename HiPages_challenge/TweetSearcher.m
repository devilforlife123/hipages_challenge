//
//  TweetSearcher.m
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import "TweetSearcher.h"


@interface TweetSearcher()

@end


@implementation TweetSearcher


static TweetSearcher* sharedInstance = nil;

+ (TweetSearcher *) sharedManager {
    static dispatch_once_t dispatchOncePredicate = 0;
    dispatch_once(&dispatchOncePredicate, ^{
        sharedInstance = [[super alloc] init];
    });
    return sharedInstance;
}

-(void)searchTweetsForTerm:(NSString *)searchTerm completion:(void (^)(NSMutableArray *, NSError *))completionBlock{
    
    NSString * encodedQuery  = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
             
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json"];
    
    NSDictionary * params = [NSDictionary new];
             
    if([searchTerm  isEqual: @""]){
        params = [NSDictionary dictionaryWithObjectsAndKeys:
                                     @"hipages", @"q",
                                     @"true", @"include_entities",
                                     @"recent", @"result_type",
                                     @"100", @"count", nil];
    }else{
        params = @{@"count" : @100,@"q" : encodedQuery};
    }
            
    SLRequest * request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                              requestMethod:SLRequestMethodGET
                                              URL:url
    parameters:params];
             
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    request.account = appDelegate.twitterAccount;
    NSURLRequest * searchRequest = [request preparedURLRequest];
    NSMutableArray * tweetsArray = [NSMutableArray new];
    
    [NSURLConnection sendAsynchronousRequest:searchRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError * error) {
        if(error != nil){
            completionBlock(nil,error);
        }
        
        NSError * jsonError = nil;
        NSDictionary * jsonDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if(jsonError != nil){
            completionBlock(nil,jsonError);
        }
        NSArray * results = [jsonDictionary valueForKey:@"statuses"];
        for(id result in results){
            [tweetsArray addObject:[TweetSearchResult tweetWithDictionary:result]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            completionBlock(tweetsArray,nil);
        });
    }];
}


@end
