//
//  DetailViewController.h
//  HiPages
//
//  Created by suraj poudel on 24/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TweetSearchResult.h"

@interface DetailViewController : UIViewController

@property(nonatomic,strong)TweetSearchResult * tweet;
@property(nonatomic,weak)IBOutlet UIImageView * imageView;
@property(nonatomic,weak)IBOutlet UILabel * label;

@end
