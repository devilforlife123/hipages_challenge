//
//  AppDelegate.h
//  HiPages_challenge
//
//  Created by suraj poudel on 26/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Accounts;
@import Social;

#define AccountTwitterAccountAccessGranted        @"TwitterAccountAccessGranted"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ACAccountStore * accountStore;
@property (strong, nonatomic) ACAccount * twitterAccount;

-(void)getTwitterAccount;

@end

