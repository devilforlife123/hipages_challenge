//
//  TweetCell.h
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweetCell : UITableViewCell

@property(nonatomic,weak)IBOutlet UIImageView * profilePicture;
@property(nonatomic,weak)IBOutlet UILabel * tweet;

@end
