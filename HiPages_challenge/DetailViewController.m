//
//  DetailViewController.m
//  HiPages
//
//  Created by suraj poudel on 24/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import "DetailViewController.h"

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setImageViewImage];
    [self setTextViewText];
}

- (void)setImageViewImage {
    [self.imageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.tweet.pictureImageUrl]]]];
}

- (void)setTextViewText {
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] init];
    [self addDescriptionTextTo:text];
    self.label.attributedText = text;
}


- (void)addDescriptionTextTo:(NSMutableAttributedString *)mutableString {
    NSString *text = self.tweet.tweetContent;
    
    if (!text.length > 0) {
        return;
        
    } else {
        text = [NSString stringWithFormat:@"\n%@", text];
        
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0f]};
    NSAttributedString *attText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
    [mutableString appendAttributedString:attText];
}

@end
