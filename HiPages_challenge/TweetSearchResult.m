//
//  flickrSearchResult.m
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import "TweetSearchResult.h"

@implementation TweetSearchResult

+(TweetSearchResult*)tweetWithDictionary:(NSDictionary*)dict{
    
    TweetSearchResult * tweet = [[TweetSearchResult alloc]init];
    tweet.tweetContent = [dict objectForKey:@"text"];
    NSDictionary * userDictionary = [dict objectForKey:@"user"];
    NSString * pictureImageURLString;
    
    pictureImageURLString = [userDictionary objectForKey:@"profile_image_url"];
    pictureImageURLString = [pictureImageURLString stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
    tweet.pictureImageUrl = pictureImageURLString;
    
    return tweet;
}

@end
