//
//  AppDelegate.m
//  HiPages_challenge
//
//  Created by suraj poudel on 26/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import "AppDelegate.h"

#define AccountTwitterSelectedAccountIdentifier @"TwitterAccountSelectedAccountIdentifier"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.accountStore = [[ACAccountStore alloc]init];
    return YES;
}

- (void)getTwitterAccount {
    
    ACAccountType *twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier: ACAccountTypeIdentifierTwitter];
    
    __weak AppDelegate *weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.accountStore requestAccessToAccountsWithType:twitterAccountType
                                                       options:nil
                                                    completion:^(BOOL granted, NSError *error) {
                                                        
                                                        NSArray *twitterAccounts = [weakSelf.accountStore accountsWithAccountType:twitterAccountType];
                                                        
                                                        if (granted && (twitterAccounts.count > 0)) {
                                                            
                                                            NSString *twitterAccountIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:AccountTwitterSelectedAccountIdentifier];
                                                            weakSelf.twitterAccount = [weakSelf.accountStore accountWithIdentifier:twitterAccountIdentifier];
                                                            
                                                            
                                                            if (weakSelf.twitterAccount) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:AccountTwitterAccountAccessGranted object:nil];
                                                                });
                                                            } else {
                                                                
                                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:AccountTwitterSelectedAccountIdentifier];
                                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                                
                                                                
                                                                if (twitterAccounts.count > 1) {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Twitter"
                                                                                                                                                 message:@"Select one of your Twitter Accounts"
                                                                                                                                          preferredStyle:UIAlertControllerStyleAlert];
                                                                        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
                                                                        
                                                                        for (ACAccount *account in twitterAccounts) {
                                                                            UIAlertAction *action = [UIAlertAction actionWithTitle:account.accountDescription style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                                                weakSelf.twitterAccount = account;
                                                                                
                                                                                [[NSUserDefaults standardUserDefaults] setObject:weakSelf.twitterAccount.identifier forKey:AccountTwitterSelectedAccountIdentifier];
                                                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                                                
                                                                                [[NSNotificationCenter defaultCenter] postNotificationName:AccountTwitterAccountAccessGranted object:nil];
                                                                            }];
                                                                            [alertController addAction:action];
                                                                        }
                                                                        [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                                                                    });
                                                                }
                                                                else {
                                                                    weakSelf.twitterAccount = [twitterAccounts lastObject];
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:AccountTwitterAccountAccessGranted
                                                                                                                            object:nil];
                                                                    });
                                                                }
                                                            }
                                                        } else {
                                                            NSLog(@"There is a problem");
                                                            if (error) {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Twitter"
                                                                                                                                             message:@"There was an error retrieving your Twitter account, make sure you have an account setup in Settings and that access is granted for HiPages"
                                                                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                                                    [alertController addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil]];
                                                                    
                                                                    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                                                                });
                                                            } else {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Twitter"
                                                                                                                                             message:@"There was an error retrieving your Twitter account, make sure you have an account setup in Settings and that access is granted for HiPages"
                                                                                                                                      preferredStyle:UIAlertControllerStyleAlert];
                                                                    [alertController addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil]];
                                                                    
                                                                    [self.window.rootViewController presentViewController:alertController animated:YES completion:nil];
                                                                });
                                                            }
                                                        }
                                                    }];
    });
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    NSLog(@"Application becomes active again");
    if(self.twitterAccount == nil){
        [self getTwitterAccount];
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
