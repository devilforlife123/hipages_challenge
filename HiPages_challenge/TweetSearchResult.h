//
//  flickrSearchResult.h
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TweetSearchResult : NSObject

@property(nonatomic,copy)NSString * tweetContent;
@property(nonatomic,copy)NSString * pictureImageUrl;

+(TweetSearchResult*)tweetWithDictionary:(NSDictionary*)dict;

@end
