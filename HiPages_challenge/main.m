//
//  main.m
//  HiPages_challenge
//
//  Created by suraj poudel on 26/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
