//
//  UIImageView+Async.m
//  HiPages
//
//  Created by suraj poudel on 23/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import "UIImageView+Async.h"

@implementation UIImageView (Async)

static NSCache * asyncImageCache = nil;

-(void)setASyncImageURL:(NSString *)path{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        asyncImageCache  = [[NSCache alloc]init];
    });
    
    if([[asyncImageCache objectForKey:path]isKindOfClass:[UIImage class]]){
        self.image = [asyncImageCache objectForKey:path];
    }
    
    __weak UIImageView * weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:path]]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView * strongSelf = weakSelf;
            if(strongSelf){
                strongSelf.image = image;
            }
        });
    });
    
}
@end
