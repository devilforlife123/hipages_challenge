//
//  UIImageView+Async.h
//  HiPages
//
//  Created by suraj poudel on 23/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Async)

-(void)setASyncImageURL:(NSString*)path;


@end
