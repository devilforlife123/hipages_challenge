//
//  MainViewController.m
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import "MainViewController.h"
#import "DetailViewController.h"
#import "TweetCell.h"
#import "TweetSearchResult.h"
#import "TweetSearcher.h"
#import "UIImageView+Async.h"

@interface MainViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,weak)IBOutlet UITableView * tweetsTable;
@property(nonatomic,weak)IBOutlet UISearchBar * searchBar;
@property(nonatomic,weak)IBOutlet UIActivityIndicatorView * activityIndicator;
@property(nonatomic,strong)UIRefreshControl * refreshControl;
@property(nonatomic,strong)NSMutableArray * searchResults;
@property(nonatomic,strong)TweetSearchResult * tweetSearchResult;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchResults = [NSMutableArray array];
    UIImage *image = [UIImage imageNamed: @"hi_pages.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    self.navigationItem.titleView = imageView;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Search Tweets" style: UIBarButtonItemStylePlain target:nil action:nil];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData:) name:AccountTwitterAccountAccessGranted object:nil];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    if (appDelegate.twitterAccount) {
         [self performSelector:@selector(refreshData:) withObject:nil afterDelay:1.0];
    } else {
        [appDelegate getTwitterAccount];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewWillDisappear:animated];
}


-(IBAction)refreshData:(id)sender
{
    
    self.searchBar.hidden = true;
    [self.activityIndicator startAnimating];
    
    [[TweetSearcher sharedManager] searchTweetsForTerm:self.searchBar.text completion:^(NSMutableArray * result, NSError *error) {
        self.searchBar.hidden = false;
        [self.activityIndicator stopAnimating];
        if(error != nil){
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Twitter Error" message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alertController animated:true completion:nil];
        }
        
        self.searchResults = result;
        [self.tweetsTable reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tweetsTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_searchResults count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    TweetCell * cell  = (TweetCell*)[tableView dequeueReusableCellWithIdentifier:@"TweetCell" forIndexPath:indexPath];
    
    _tweetSearchResult = _searchResults[indexPath.row];
    [cell.profilePicture setImageWithURL:[NSURL URLWithString:_tweetSearchResult.pictureImageUrl]];
    cell.tweet.text = _tweetSearchResult.tweetContent;
    
    return cell;
}

-(BOOL)isLandscapeOrientation{
    
    return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication]statusBarOrientation]);
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication]statusBarOrientation])) {
        
        return [self hasImageAtIndexPath:indexPath] ? 140.0 : 120.0;
    }else{
        return [self hasImageAtIndexPath:indexPath] ? 235.0 : 155.0;
    }
}


-(BOOL)hasImageAtIndexPath:(NSIndexPath*)indexPath{
    TweetSearchResult * tweetSearchResult = _searchResults[indexPath.row];
    if(tweetSearchResult.pictureImageUrl != nil){
        return true;
    }else{
        return false;
    }
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
    [self refreshData:searchBar];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.searchBar.text = @"";
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tweetsTable indexPathForSelectedRow];
    TweetSearchResult * tweet = self.searchResults[indexPath.row];
    
    DetailViewController * viewController = [segue destinationViewController];
    viewController.tweet = tweet;
}





@end
