//
//  TweetSearcher.h
//  HiPages
//
//  Created by suraj poudel on 22/06/2015.
//  Copyright (c) 2015 PyroTech Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TweetSearchResult.h"
#import "AppDelegate.h"

@interface TweetSearcher : NSObject

+ (TweetSearcher *) sharedManager;
-(void)searchTweetsForTerm:(NSString*)searchTerm completion:(void(^)(NSMutableArray * result,NSError *error))completionBlock;

@end
